def tf_hash = [:]
pipeline {
    agent {
        node {
            label 'drone'
        }
    }
    
    environment {
        BOOTSRAP_HASH_PASS  = credentials('VAULT_JENKINS_PROXMOX_BOOTSTRAP_USERPASS')
        PVE_CRED            = credentials('VAULT_JENKINS_PROXMOX_USERPASS')
        REPO_DIR            = "repo"
        TF_VARS             = "TF_VAR_pve_password='${PVE_CRED_PSW}' TF_VAR_template_password='${BOOTSRAP_HASH_PASS_PSW}'"
        workspace           = "${WORKSPACE}"
        REALM               = "FWC.DC.OPS.VM"
    }
    
    parameters {
        string(
            name: 'GIT_REPO',
            defaultValue: '',
            description: 'Git repository container terraform files'
        )
        string(
            name: '_SLACK_CHANNEL',
            defaultValue: 'jenkins-pipeline-pve-vm-create',
            description: 'Slack channel to receive notifications'
        )
        credentials credentialType: 'com.datapipe.jenkins.vault.credentials.common.VaultSSHUserPrivateKeyImpl',
            defaultValue: 'VAULT_JENKINS_GITLAB_SSHKEY',
            name: 'GIT_CREDS', 
            required: true
        booleanParam(
            name: 'IPA_REGISTER',
            defaultValue: false,
            description: 'FreeIPA registration'
        )
        booleanParam(
            name: 'IPA_GETAPPCERT',
            defaultValue: false,
            description: 'Request application cert'
        )
    }

    options {
        ansiColor('gnome-terminal')
        buildDiscarder(logRotator(daysToKeepStr: '7', numToKeepStr: '20'))
    }

    stages {
        stage('Clean repo folder') {
            steps {
                dir(workspace) {
                    script {
                        sh """
                        rm -rf ${REPO_DIR}
                        """
                    }
                }
            }
        }

        stage('Clone') {
            environment {
                GIT_SSH_COMMAND = "ssh -o StrictHostKeyChecking=no"
            }
            steps {
                checkout([  
                    $class: 'GitSCM', 
                    branches: [[name: 'refs/heads/main']], 
                    doGenerateSubmoduleConfigurations: false, 
                    extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: REPO_DIR]], 
                    submoduleCfg: [], 
                    userRemoteConfigs: [[credentialsId: GIT_CREDS.toString(), url: params.GIT_REPO]]
                ])
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        sh """
                        git status
                        """
                    }
                }
            }
        }

        stage('Init') {
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        sh """
                        ${TF_VARS} terraform init
                        """
                    }
                }
                
            }
        }

        stage('Validate') {
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        sh """
                        terraform validate
                        """
                    }
                }
            }
        }

        stage('Plan create') {
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        PLAN_PARAMS = "plan --out '${env.BUILD_NUMBER}.tfbin'"
                        sh """
                        ${TF_VARS} terraform ${PLAN_PARAMS}
                        """
                    }
                }
            }
        }

        stage('Apply create') {
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        sh """
                        terraform apply '${env.BUILD_NUMBER}.tfbin'
                        """

                        _MESSAGE = """
                        TERRAFORM APPLY SUCCESSFUL!
                        More info at: ${env.BUILD_URL}
                        """
                        sendSlackMessage(params._SLACK_CHANNEL,currentBuild.currentResult,_MESSAGE)
                    }
                }
            }
        }

        stage('Update SCM') {
            environment {
                GIT_SSH_COMMAND = "ssh -o StrictHostKeyChecking=no"
            }
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        catchError(buildResult: 'SUCCESS', stageResult: 'ABORTED') {
                            // DIFF FILES
                            _GIT_FILE_LIST = sh(
                                script: "git diff-files --name-only",
                                returnStdout: true
                            )
                            for (_FILE in _GIT_FILE_LIST.split("\n")) {
                                sh("git add ${_FILE}")
                            }
                            // NEW FILES
                            _GIT_FILE_LIST = sh(
                                script: "git ls-files . --exclude-standard --others",
                                returnStdout: true
                            )
                            for (_FILE in _GIT_FILE_LIST.split("\n")) {
                                sh("git add ${_FILE}")
                            }
                            sh("git commit -m 'PIPELINE: ${env.JOB_NAME} BUILD: ${env.BUILD_NUMBER}'")
                            sh("GIT_SSH_COMMAND='ssh -o StrictHostKeyChecking=no' git push origin HEAD:main")
                        }
                    }
                }
            }
        }

        stage('Register DNS') {
            steps {
                dir("${workspace}/${REPO_DIR}") {
                    script {
                        // OBTAIN TERRAFORM OUTPUT
                        _TF_HOSTNAME = sh(
                            script: "terraform output --json instance_hostname | jq -r '.[] | .[] '",
                            returnStdout: true
                        ).split("\n")
                        _TF_IPV4 = sh(
                            script: "terraform output --json instance_ipv4_addr | jq -r '.[] | .[] '",
                            returnStdout: true
                        ).split("\n")
                        _TF_DOMAIN = sh(
                            script: "terraform output --json instance_domainname | jq -r '.[] | .[] '",
                            returnStdout: true
                        ).split("\n")

                        def branches = [:]
                        for (int i=0; i<_TF_HOSTNAME.size();i++) {
                            def instance_hostname = _TF_HOSTNAME[i]
                            def instance_ipv4_addr = _TF_IPV4[i]
                            def instance_domainname = _TF_DOMAIN[i]
                            println("instance_hostname:     ${instance_hostname}")
                            println("instance_ipv4_addr:    ${instance_ipv4_addr}")
                            println("instance_domainname:   ${instance_domainname}")

                            branches[instance_hostname] = {
                                build job: 'provisioning/pipeline-ipaclient-registration',
                                    parameters: [
                                        string(
                                            name: 'HOSTNAME', 
                                            value: instance_hostname
                                        ),
                                        string(
                                            name: 'HOST_IP', 
                                            value: instance_ipv4_addr
                                        ),
                                        string(
                                            name: 'DOMAIN',
                                            value: instance_domainname
                                        ),
                                        string(
                                            name: 'REALM',
                                            value: REALM
                                        ),
                                        string(
                                            name: '_SLACK_CHANNEL', 
                                            value: params._SLACK_CHANNEL
                                        ),
                                        booleanParam(
                                            name: 'IPA_GETAPPCERT',
                                            value: params.IPA_GETAPPCERT
                                        ),
                                    ],
                                    propagate: true,
                                    quietPeriod: 1,
                                    wait: true
                            }
                        }
                        parallel branches
                    }
                }
            }
        }

        stage('DNS Lookup') {
            steps {
                script {
                    for (int i=0; i<_TF_IPV4.size();i++) {
                        sh("nslookup ${_TF_HOSTNAME[i]}.${_TF_DOMAIN[i]}")
                    }
                }
            }
        }
    }
    post {
        always {
            script {
                _MESSAGE = """
                    *${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}
                    GIT_URL: ${env.GIT_URL}
                    GIT_BRANCH: ${env.GIT_BRANCH}
                    GIT_COMMIT: ${env.GIT_COMMIT}
                    More info at: ${env.BUILD_URL}
                """
            }
            sendSlackMessage(params._SLACK_CHANNEL,currentBuild.currentResult,_MESSAGE)
        }
        cleanup {
            cleanWs()
        }
    }
}

Void sendBuild(String _HOSTNAME, String _IPv4, String _DOMAIN, String _REALM, String _CHANNEL, Boolean _GETAPPCERT) {
    // Ping ipv4
    sh("ping -c 4 ${_IPv4}")
    println(_HOSTNAME)
    println(_IPv4)
    
    // call ipa registration
    build job: 'provisioning/freeipa-client-registration',
        parameters: [
            string(
                name: 'HOSTNAME', 
                value: _HOSTNAME),
            string(
                name: 'HOST_IP', 
                value: _IPv4),
            string(
                name: 'DOMAIN',
                value: _DOMAIN),
            string(
                name: 'REALM',
                value: REALM),
            string(
                name: '_SLACK_CHANNEL', 
                value: _CHANNEL),
            booleanParam(
                name: 'IPA_GETAPPCERT',
                value: _GETAPPCERT),
        ],
        propagate: true,
        wait: true
}

Void sendSlackMessage(String _CHANNEL, String _RESULT, String _MESSAGE) {
    COLOR_MAP = [
        'SUCCESS': 'good',
        'FAILURE': 'danger',
    ]
    slackSend channel: _CHANNEL,
        color: COLOR_MAP[_RESULT],
        message: _MESSAGE
}