# Jenkins pipeline PVE VM create

Declarative Jenkins pipeline responsible for the following:
- Creation of Proxmox Virtual machine using terraform.
- Registration of a Newly deployed virtual machine to a FreeIPA server
- Configuration for automated SSL deployments

### Input Parameters
| Parameter | Description |
| --- | --- |
| `GIT_REPO` | Git repository containing terraform config files |
| `GIT_CREDS` | SSH credentials granting access to git repository |
|||

### Pipeline Steps
| Step | Description |
| --- | --- |
| Declarative: Checkout SCM | Pull this repository to refresh Pipeline code |
| Clean repo folder | `rm -rf ${REPO_DIR}` folder used to store target repository |
| Clone | Clone `${GIT_REPO}` target to `${REPO_DIR}` folder |
| Init | Initialise terraform dependencies|
| Validate | Validate terraform configuration |
| Plan create | Execute terraform plan storing the plan to a plan file `${env.BUILD_NUMBER}` |
| Apply create | Execute terraform apply `${env.BUILD_NUMBER}` |
| Register DNS | [Downstream pipeline](https://git.fwc.dc.ops.vm/vanir/jenkins-pipeline-ipaclient-registration/-/blob/main/README.md) allowing the vm to be registered to the FreeIPA server |
| DNS Lookup | Post DNS registration test |
| Update SCM | Update `${GIT_REPO}` code with the new terraform statefiles |
|||
